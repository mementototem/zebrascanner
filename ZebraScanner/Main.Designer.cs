﻿namespace ZebraCodeReader
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.btnReadCode = new System.Windows.Forms.Button();
            this.btnToClipBoard = new System.Windows.Forms.Button();
            this.textResult = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnReadCode
            // 
            this.btnReadCode.Location = new System.Drawing.Point(12, 12);
            this.btnReadCode.Name = "btnReadCode";
            this.btnReadCode.Size = new System.Drawing.Size(192, 23);
            this.btnReadCode.TabIndex = 0;
            this.btnReadCode.Text = "Read most recent barcode";
            this.btnReadCode.UseVisualStyleBackColor = true;
            this.btnReadCode.Click += new System.EventHandler(this.btnReadCode_Click);
            // 
            // btnToClipBoard
            // 
            this.btnToClipBoard.Location = new System.Drawing.Point(210, 12);
            this.btnToClipBoard.Name = "btnToClipBoard";
            this.btnToClipBoard.Size = new System.Drawing.Size(116, 23);
            this.btnToClipBoard.TabIndex = 1;
            this.btnToClipBoard.Text = "Copy to Clipboard";
            this.btnToClipBoard.UseVisualStyleBackColor = true;
            this.btnToClipBoard.Click += new System.EventHandler(this.btnToClipBoard_Click);
            // 
            // textResult
            // 
            this.textResult.Location = new System.Drawing.Point(13, 42);
            this.textResult.Multiline = true;
            this.textResult.Name = "textResult";
            this.textResult.ReadOnly = true;
            this.textResult.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.textResult.Size = new System.Drawing.Size(313, 293);
            this.textResult.TabIndex = 2;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(337, 347);
            this.Controls.Add(this.textResult);
            this.Controls.Add(this.btnToClipBoard);
            this.Controls.Add(this.btnReadCode);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Main";
            this.Text = "Zebra Code Reader";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnReadCode;
        private System.Windows.Forms.Button btnToClipBoard;
        private System.Windows.Forms.TextBox textResult;
    }
}

