﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Windows.Forms;
using OnBarcode.Barcode.BarcodeScanner;

namespace ZebraCodeReader
{
    public partial class Main : Form
    {
        private String filePath = "";
        private String sep = Path.DirectorySeparatorChar.ToString();
        private String output = "";
        private String[] barcodes = null;
        private static int currentBarcode = 0;

        public Main()
        {
            DateTime today = DateTime.Today;
            String thisDay = today.ToString("yyyy-MM-dd");
            String picturePath = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);

            InitializeComponent();

            filePath = picturePath + sep + thisDay;
        }

        private void btnReadCode_Click(object sender, EventArgs e)
        {
            DirectoryInfo dir = new DirectoryInfo(filePath);

            // Check file existing
            bool dirExist = Directory.Exists(dir.ToString());

            if (dirExist == false)
            {
                output = dir.ToString() + " don't exist.";
                textResult.Text = output;
                return;
            }

            String recentFile = filePath + sep + dir.GetFiles().OrderByDescending(f => f.LastWriteTime).First().ToString();
            String[] rawBarcodes = BarcodeScanner.Scan(recentFile, BarcodeType.Code128);
            int usableBarcodes = rawBarcodes.Length - 1;

            barcodes = new String[usableBarcodes];
            Array.Copy(rawBarcodes, 1, barcodes, 0, usableBarcodes);

            output = "Reading barcode on...\r\n";
            output += recentFile;
            output += "\r\n===================================================\r\n\r\n";

            for (int i = 0; i < usableBarcodes; i++)
            {
                output += "Barcode " + (i + 1).ToString() + ":\r\n\r\n";
                output += barcodes[i];
                output += "\r\n\r\n\r\n\r\n";
            }

            textResult.Text = output;

            currentBarcode = 0;
        }

        private void btnToClipBoard_Click(object sender, EventArgs e)
        {
            if (barcodes == null || barcodes.Length == 0)
            {
                MessageBox.Show("Barcode does not exist", "No Barcode", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            Clipboard.SetText(barcodes[currentBarcode]);
            currentBarcode++;
        }
    }
}
